//exprlp.cpp, Copyright (c) 2002-2008 R.Lackner
//
//    This file is part of RLPlot.
//
//    RLPlot is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    RLPlot is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with RLPlot; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// A console application to process *.rlp files

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <locale.h>
#include <unistd.h>			//required for unlink()
#include "Version.h"
#include "rlplot.h"

int file_fmt = FF_UNKNOWN;
bool bQuiet = false, bSVGtype = false, bDelete = false;
char *szFile1 = 0L, *szFile2 = 0L;
int dlgtxtheight = 12;				//stub: not used
char *name1, *name2;				//the filenames

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// direct messages to console
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void InfoBox(char *Msg)
{
	if(!bQuiet) fprintf(stderr, "exprlp INFO: %s\n", Msg);
}

void ErrorBox(char *Msg)
{
	if(!bQuiet) fprintf(stderr, "exprlp ERROR: %s\n", Msg);
}

bool YesNoBox(char *Msg)
{
return false;
}

int YesNoCancelBox(char *Msg)
{
	return 0;
}

void HideCopyMark()
{
}

void ShowCopyMark(anyOutput *out, RECT *mrk, int nRec)
{
}

void CopyText(char *txt, int len)
{
}

unsigned char* PasteText()
{
	return 0L;
}


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// create a root object to handle I/O
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class ExpRoot:public GraphObj{
public:
	ExpRoot(char *file1, char *file2);
	~ExpRoot();
	bool Command(int cmd, void *tmpl, anyOutput *o);

private:
	GraphObj *go;
};

ExpRoot::ExpRoot(char *file1, char *file2):GraphObj(0L, 0L)
{
	if(file1 && strcmp("-", file1)) name1 = file1;
	else name1 = 0L;
	if(file2 && strcmp("-", file2)) name2 = file2;
	else name2 = 0L;
	go = 0L;
	OpenGraph(this, name1, 0L, false);
	if(bDelete && name1 && name1[0]) unlink(name1);
}

ExpRoot::~ExpRoot()
{
	if(go) {
		DeleteGO(go);
		if(!bQuiet)fprintf(stderr, "Object deleted after read\n");
		}
}

bool
ExpRoot::Command(int cmd, void *tmpl, anyOutput *o)
{
	int i;

	switch(cmd) {
	case CMD_DROP_GRAPH:
		go = (GraphObj*)tmpl;
		if(go) {
			go->Command(CMD_SET_DATAOBJ, 0L, 0L);
			switch(file_fmt){
			case FF_SVG:
				DoExportSvg(go, name2, bSVGtype ? 1L : 0L);
				break;
			case FF_WMF:
				DoExportWmf(go, name2, 600.0f, 0L);
				break;
			case FF_EPS:
				DoExportEps(go, name2, 0L);
				break;
			case FF_RLP:
				SaveGraphAs(go);
				break;
			default:
				ErrorBox("Unknown file extension or format of destination");
				}
			DeleteGO(go);
			go = 0L;
			}
		break;
		}
	return true;
}


int Usage()
{
	printf("______________________________________________________________\n");
	printf("\nexprlp: RLPlot export utility, version %s.\n", SZ_VERSION);
	printf("Copyright (C) 2002-2005 R. Lackner\n");
	printf("This is free software published under the GNU\n");
	printf("general public licence (GPL).\n");
	printf("\nUsage: exprlp [options] <input> [options] [<output>]\n");
	printf("\nOptions:\n");
	printf("   -      use stdin/stdout as input or output file; requires\n");
	printf("             that file format is set by -e | -s | -w option\n");
	printf("             not an option in the strict sense\n");
	printf("   -h     print this information\n");
	printf("   -d     delete input file after read\n");
	printf("   -e     output Encapsulated PostScript, *.eps\n");
	printf("   -s     output Scalable Vector Graphics, *.svg\n");
	printf("   -S     like -s, start output with \"Content-Type: image/svg+xml\"\n");
	printf("   -v     print RLPlot version\n");
	printf("   -w     output Windows Meta File, *.wmf\n");
	printf("   -q     quiet mode: suppress output to the console\n");
	printf("\nExamples:\n");
	printf("   exprlp foo.rlp foo.svg      ;exports Scalable Vector Graphics\n");
	printf("   exprlp -q foo.rlp foo.eps   ;exports Encapsulated PostScript, no messages\n");
	printf("   exprlp foo.rlp foo.wmf      ;exports Windows Meta File\n");
	printf("   exprlp -sq foo.rlp -        ;exports SVG to the console, no messages\n");
	printf("   exprlp -eq - -              ;converts inputfile from stdin to EPS\n");
	printf("                                   on stdout: useful for pipes\n");
	printf("\n switch character is either \'-\' or \'/\'\n");
	printf("______________________________________________________________\n\n");
	if(szFile1) free(szFile1);	if(szFile2) free(szFile2);
	szFile1 = szFile2 = 0L;
	return 1;
}

int main (int argc, char **argv)
{
	ExpRoot *base = 0L;
	int i, j, k;

	for (i = 1, j = 0; i < argc; i++) {
		if(argv[i][0] == '-' || (argv[i][0] == '/' && strlen(argv[i]) < 5)) {
			//check for switch
			for(k = 1; argv[i][k-1]; k++) {
				switch(argv[i][k]){
				case 'h':	case 'H':	case '?':
					return Usage();
				case 'd':
					bDelete = true;
					break;
				case 'S':
					bSVGtype = true;
				case 's':
					file_fmt = FF_SVG;
					break;
				case 'e':	case 'E':
					file_fmt = FF_EPS;
					break;
				case 'r':	case 'R':
					file_fmt = FF_RLP;
					break;
				case 'w':	case 'W':
					file_fmt = FF_WMF;
					break;
				case 'q':	case 'Q':
					bQuiet = true;
					break;
				case 'v':	case 'V':
					printf("RLPlot version %s\n", SZ_VERSION);
					return 0;
				case '\0':
					if(k == 1) {
						if(j == 0) szFile1 = strdup("-");
						else if(j == 1) szFile2 = strdup("-");
						j++;
						}
					break;
					}
				}
			}
		else switch(j) {
		case 0:
			szFile1 = strdup(argv[i]);
			j++;
			break;
		case 1:
			szFile2 = strdup(argv[i]);
			j++;
			}
		}
	if(file_fmt == FF_UNKNOWN && szFile2 && (i = strlen(szFile2)) > 4) {
		if(0==strcmp(".svg", szFile2+i-4)) file_fmt = FF_SVG;
		else if(0==strcmp(".wmf", szFile2+i-4)) file_fmt = FF_WMF;
		else if(0==strcmp(".eps", szFile2+i-4)) file_fmt = FF_EPS;
		else if(0==strcmp(".rlp", szFile2+i-4)) file_fmt = FF_RLP;
		}
	if(file_fmt == FF_UNKNOWN) {
		if(szFile1)printf("\n**** Unknown file extension or format ****\n\n");
		return Usage();
		}
	if(!bQuiet) {
		fprintf(stderr,"Input file \"%s\"\n", szFile1);
		fprintf(stderr,"Output file \"%s\"\n", szFile2);
		}
	if(!szFile1) return Usage();
	base = new ExpRoot(szFile1, szFile2);
	if(base) {
		delete base;
		}
	if(szFile1) free(szFile1);	if(szFile2) free(szFile2);
	return 0;
}
