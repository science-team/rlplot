//TheDialog.h, Copyright (c) 2001-2008 R.Lackner
//Definitions for TheDialog.cpp
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
//    This file is part of RLPlot.
//
//    RLPlot is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    RLPlot is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with RLPlot; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// declarations relevant for user
typedef struct {
	unsigned int id;		//consecutive number
	unsigned int next;		//id of next object
	unsigned int first;		//number of first child
	unsigned long flags;		//any status flag bits
	unsigned int type;		//identifier of dialog item
	void *ptype;			//pointer to information dependend on type
	int x, y, w, h;			//start coordinates, width, height	
} DlgInfo;

//defining a tab
typedef struct {
	unsigned int x1, x2;		//relative position to left border
	unsigned int height;		//the height
	char *text;					//descriptor shown on tab
} TabSHEET;

//types of dialogs
enum {NONE, PUSHBUTTON, ARROWBUTT, COLBUTT, FILLBUTTON, SHADE3D, LINEBUTT, SYMBUTT,
	FILLRADIO, SYMRADIO, CHECKBOX, RADIO0, RADIO1, RADIO2, LTEXT, RTEXT, CTEXT, EDTEXT, 
	RANGEINPUT, EDVAL1, INCDECVAL1, HSCROLL, VSCROLL, TXTHSP, ICON, GROUP, 
	GROUPBOX, SHEET, ODBUTTON, LISTBOX1, TREEVIEW, LINEPAT, TEXTBOX, CHECKPIN, TRASH,
	CONFIG};

//flags
#define CHECKED      0x00000001L
#define TOUCHEXIT    0x00000002L
#define TOUCHSELEXIT 0x00000004L
#define ISRADIO      0x00000008L
#define ISPARENT     0x00000010L
#define OWNDIALOG    0x00000020L
#define DEFAULT      0x00000040L
#define HIDDEN       0x00000080L
#define NOSELECT     0x00000100L
#define HREF         0x00000200L
#define NOEDIT       0x00000400L
#define LASTOBJ      0x00100000L

#define EXRADIO      TOUCHEXIT|ISRADIO
#define ODEXIT       OWNDIALOG|TOUCHEXIT

//owner draw button commands
enum {OD_CREATE, OD_DELETE, OD_DRAWNORMAL, OD_DRAWSELECTED, OD_SELECT, OD_MBTRACK,
	OD_SETLINE, OD_GETLINE, OD_SETFILL, OD_GETFILL, OD_ACCEPT};

class tag_DlgObj{
public:
	RECT cr, hcr;
	int Id, type;
	unsigned long flags;
	bool bChecked, bLBdown, bActive, bModal;

	virtual bool Command(int cmd, void *tmpl, anyOutput *o){return false;};
	virtual void DoPlot(anyOutput *o) {return;};
	virtual bool Select(int x, int y, anyOutput *o) {return false;};
	virtual bool GetColor(int id, DWORD *color) {return false;};
	virtual void SetColor(int id, DWORD color) {return;};
	virtual bool GetValue(int id, double *val) {return false;};
	virtual bool GetInt(int id, int *val) {return false;};
	virtual bool SetCheck(int id, anyOutput *o, bool state) {return false;};
	virtual bool GetCheck(int Id) {return bChecked;};
	virtual bool GetText(int id, char *txt, int size) {return false;};
	virtual void MBtrack(MouseEvent *mev, anyOutput *o) {return;};
	virtual void Activate(int id, bool active){return;};
};

class Dialog:public tag_DlgObj {
public:
	tag_DlgObj *parent;
	LineDEF Line;
	FillDEF Fill;
	TextDEF TextDef;

	Dialog(tag_DlgObj *par, DlgInfo * desc, RECT rec);
	virtual bool Select(int x, int y, anyOutput *o);
	bool SetCheck(int id, anyOutput *o, bool state);
	void MBtrack(MouseEvent *mev, anyOutput *o);
	virtual void Activate(int id, bool active);
};

typedef struct {
	unsigned int id;			//consecutive number
	unsigned int next;		//id of next object
	unsigned int first;		//number of first child
	unsigned long flags;		//any status flag bits
	Dialog *dialog;			//pointer to dialog object
} DlgTmpl;

class DlgRoot:public tag_DlgObj {
public:
	anyOutput *CurrDisp;		//the dialog's output class
	void *hDialog;			//handle to the dialog window/widget

	DlgRoot(DlgInfo *tmpl, DataObj *d);
	~DlgRoot();
	bool Command(int cmd, void *tmpl, anyOutput *o);
	void DoPlot(anyOutput *o);
	bool CurUpDown(int cmd);
	bool GetColor(int id, DWORD *color);
	void SetColor(int id, DWORD color);
	bool GetValue(int id, double *val);
	bool GetInt(int id, int *val);
	bool SetCheck(int id, anyOutput *o, bool state);
	bool GetCheck(int Id);
	void Activate(int id, bool active);
	bool GetText(int id, char *txt, int size);
	bool SetText(int id, char *txt);
	bool SetValue(int id, double val);
	bool TextStyle(int id, int style);
	bool TextFont(int id, int font);
	bool TextSize(int id, int size);
	bool ShowItem(int id, bool show);
	int GetResult();
	int FindIndex(unsigned short id);
	void ForEach(int cmd, int start, anyOutput *o);
	bool ItemCmd(int id, int cmd, void *tmpl);
	anyOutput *GetOutputClass(){return CurrDisp;};

private:
	int res_q[256], res_put, res_get, cDlgs, cContinue;
	anyOutput *ParentOut;
	DataObj *data;
	Dialog *oldFocus, *oldDefault, *oldTabStop;
	bool bActive, bRedraw;
	GraphObj *c_go;
	Dialog **tabstops, *mrk_item;
	DlgTmpl **dlg;
	MouseEvent *mev;
};

class PushButton:public Dialog {
public:
	PushButton(tag_DlgObj *par, DlgInfo * desc, RECT rec, char *text);
	~PushButton();
	bool Command(int cmd, void *tmpl, anyOutput *o);
	void DoPlot(anyOutput *o);
	bool Select(int x, int y, anyOutput *o);

private:
	char *Text;
};

class TextBox:public Dialog {
public:
	TextBox(tag_DlgObj *par, DlgInfo * desc, RECT rec, char *text);
	~TextBox();
	bool Command(int cmd, void *tmpl, anyOutput *o);
	void DoPlot(anyOutput *o);
	bool Select(int x, int y, anyOutput *o);
	void MBtrack(MouseEvent *mev, anyOutput *o);
	bool GetText(int id, char *txt, int size);

private:
	TextFrame *cont;
};

class ArrowButton:public Dialog {
public:
	ArrowButton(tag_DlgObj *par, DlgInfo * desc, RECT rec, int *which);
	void DoPlot(anyOutput *o);
	bool Select(int x, int y, anyOutput *o);

private:
	int direct;
};

class ColorButton:public Dialog {
public:
	ColorButton(tag_DlgObj *par, DlgInfo * desc, RECT rec, DWORD *color);
	void DoPlot(anyOutput *o);
	bool Select(int x, int y, anyOutput *o);
	bool GetColor(int id, DWORD *color) {*color = col; return true;};
	void SetColor(int id, DWORD color) {col = color; return;};

private:
	DWORD col;
};

class FillButton:public Dialog {
public:
	FillButton(tag_DlgObj *par, DlgInfo * desc, RECT rec, FillDEF *fill);
	void DoPlot(anyOutput *o);
	bool Select(int x, int y, anyOutput *o);
	bool GetColor(int id, DWORD *color) {*color = CurrFill->color; return true;};

private:
	FillDEF *CurrFill;
};

class Shade3D:public Dialog {
public:
	Shade3D(tag_DlgObj *par, DlgInfo * desc, RECT rec, FillDEF *fill);
	void DoPlot(anyOutput *o);
	bool Select(int x, int y, anyOutput *o);
	bool GetColor(int id, DWORD *color) {*color = CurrFill->color; return true;};

private:
	FillDEF *CurrFill;
};

class LineButton:public Dialog {
public:
	LineButton(tag_DlgObj *par, DlgInfo * desc, RECT rec, LineDEF *line);
	void DoPlot(anyOutput *o);
	bool Select(int x, int y, anyOutput *o);

private:
	LineDEF *CurrLine;
	POINT pts[2];
};

class SymButton:public Dialog {
public:
	SymButton(tag_DlgObj *par, DlgInfo * desc, RECT rec, Symbol **sym);
	void DoPlot(anyOutput *o);
	bool Select(int x, int y, anyOutput *o);

private:
	Symbol **symbol;
};

class FillRadioButt:public Dialog {
public:
	FillRadioButt(tag_DlgObj *par, DlgInfo * desc, RECT rec, unsigned int pattern);
	void DoPlot(anyOutput *o);
	bool Select(int x, int y, anyOutput *o);
};

class SymRadioButt:public Dialog {
public:
	SymRadioButt(tag_DlgObj *par, DlgInfo * desc, RECT rec, int *type);
	~SymRadioButt();
	void DoPlot(anyOutput *o);
	bool Select(int x, int y, anyOutput *o);

private:
	Symbol *Sym;
};

class CheckBox:public Dialog {
public:
	CheckBox(tag_DlgObj *par, DlgInfo * desc, RECT rec, char *text);
	~CheckBox();
	bool Command(int cmd, void *tmpl, anyOutput *o);
	void DoPlot(anyOutput *o);
	bool Select(int x, int y, anyOutput *o);

private:
	char *Text;
};

class CheckPin:public Dialog {
public:
	CheckPin(tag_DlgObj *par, DlgInfo * desc, RECT rec);
	~CheckPin();
	bool Command(int cmd, void *tmpl, anyOutput *o);
	void DoPlot(anyOutput *o);
	bool Select(int x, int y, anyOutput *o);
};

class Trash:public Dialog {
public:
	Trash(tag_DlgObj *par, DlgInfo * desc, RECT rec);
	~Trash();
	bool Command(int cmd, void *tmpl, anyOutput *o);
	void DoPlot(anyOutput *o);
	bool Select(int x, int y, anyOutput *o);
};

class Config:public Dialog {
public:
	Config(tag_DlgObj *par, DlgInfo * desc, RECT rec);
	~Config();
	bool Command(int cmd, void *tmpl, anyOutput *o);
	void DoPlot(anyOutput *o);
	bool Select(int x, int y, anyOutput *o);
};

class RadioButton:public Dialog {
public:
	RadioButton(tag_DlgObj *par, DlgInfo * desc, RECT rec, char *text);
	~RadioButton();
	bool Command(int cmd, void *tmpl, anyOutput *o);
	void DoPlot(anyOutput *o);
	bool Select(int x, int y, anyOutput *o);
	void SetColor(int id, DWORD color);

private:
	char *Text;
};

class Text:public Dialog {
public:
	Text(tag_DlgObj *par, DlgInfo * desc, RECT rec, char *text);
	~Text();
	bool Command(int cmd, void *tmpl, anyOutput *o);
	void DoPlot(anyOutput *o);
	bool Select(int x, int y, anyOutput *o);
	void SetColor(int id, DWORD color);
	void MBtrack(MouseEvent *mev, anyOutput *o);

private:
	char *txt;
};

class InputText:public Dialog {
public:
	EditText *Text;

	InputText(tag_DlgObj *par, DlgInfo * desc, RECT rec, char *text);
	~InputText();
	virtual bool Command(int cmd, void *tmpl, anyOutput *o);
	virtual void DoPlot(anyOutput *o);
	virtual bool Select(int x, int y, anyOutput *o);
	virtual bool GetValue(int id, double *val);
	bool GetInt(int id, int *val);
	bool GetText(int id, char *txt, int size);
	virtual void MBtrack(MouseEvent *mev, anyOutput *o);
	virtual void Activate(int id, bool active);

private:
	anyOutput *Disp;
};

class RangeInput:public InputText {
public:
	RangeInput(tag_DlgObj *par, DlgInfo * desc, RECT rec, char *text, DataObj *d);
	~RangeInput();
	bool Command(int cmd, void *tmpl, anyOutput *o);
	bool Select(int x, int y, anyOutput *o);
	void Activate(int id, bool active);

private:
	DataObj *data;
};

class InputValue:public InputText {
public:
	InputValue(tag_DlgObj *par, DlgInfo * desc, RECT rec, double *value);
	~InputValue();
};

class IncDecValue:public InputText {
public:
	IncDecValue(tag_DlgObj *par, DlgInfo * desc, RECT rec, double *value);
	~IncDecValue();
	bool Command(int cmd, void *tmpl, anyOutput *o);
	void DoPlot(anyOutput *o);
	bool Select(int x, int y, anyOutput *o);
	bool GetValue(int id, double *val);
	void MBtrack(MouseEvent *mev, anyOutput *o);

private:
	ArrowButton *butts[2];
	bool hasMinMax, hasStep;
	double theMin, theMax, theStep;
};

class TxtHSP:public Dialog {
public:
	TxtHSP(tag_DlgObj *par, DlgInfo *desc, RECT rec, int *align);
	~TxtHSP();
	bool Command(int cmd, void *tmpl, anyOutput *o);
	void DoPlot(anyOutput *o);
	bool Select(int x, int y, anyOutput *o);
	bool GetInt(int id, int *val);
	void MBtrack(MouseEvent *mev, anyOutput *o);

private:
	TextDEF txt;
	RadioButton *butts[9];
	DlgInfo *d2;
};

class SlideRect:public Dialog{
public:
	int sLine;

	SlideRect(tag_DlgObj *par, DlgInfo *desc, RECT rec, bool isVert);
	~SlideRect();
	bool Command(int cmd, void *tmpl, anyOutput *o);
	void DoPlot(anyOutput *o);
	bool Select(int x, int y, anyOutput *o);
	bool GetValue(int id, double *val);
	void MBtrack(MouseEvent *mev, anyOutput *o);

private:
	int dx, dy, w, h;
	bool bV, puSel, pdSel;
	RECT buttrc, puRC, pdRC;
};

class ScrollBar:public Dialog{
public:
	int sLine, sPage;

	ScrollBar(tag_DlgObj *par, DlgInfo *desc, RECT rec, bool isVert);
	~ScrollBar();
	bool Command(int cmd, void *tmpl, anyOutput *o);
	void DoPlot(anyOutput *o);
	bool Select(int x, int y, anyOutput *o);
	bool GetValue(int id, double *val);
	void MBtrack(MouseEvent *mev, anyOutput *o);

private:
	ArrowButton *butts[3];
	SlideRect *slrc;
};

class Icon:public Dialog {
public:
	Icon(tag_DlgObj *par, DlgInfo * desc, RECT rec, int *ico);
	void DoPlot(anyOutput *o);

private:
	int icon;
};

class Group:public Dialog {
public:
	Group(tag_DlgObj *par, DlgInfo * desc, RECT rec);
	~Group();
	bool Command(int cmd, void *tmpl, anyOutput *o);
	void DoPlot(anyOutput *o);
	bool Select(int x, int y, anyOutput *o);
	void MBtrack(MouseEvent *mev, anyOutput *o);

	InputText *TextFocus;
	Dialog **Children;
	int numChildren;
};

class GroupBox:public Group {
public:
	GroupBox(tag_DlgObj *par, DlgInfo * desc, RECT rec, char *txt);
	~GroupBox();
	void DoPlot(anyOutput *o);

private:
	char *Text;
};

class TabSheet:public Group {
public:
	TabSheet(tag_DlgObj *par, DlgInfo * desc, RECT rec, TabSHEET *sh, DataObj *d);
	~TabSheet();
	void DoPlot(anyOutput *o);
	bool Select(int x, int y, anyOutput *o);

private:
	DataObj *data;
	RECT rctab;
	char *Text;
};

class ODbutton:public Dialog {
public:
	ODbutton(tag_DlgObj *par, DlgInfo *des, RECT rec, void*proc);
	~ODbutton();
	void DoPlot(anyOutput *o);
	bool Select(int x, int y, anyOutput *o);
	void MBtrack(MouseEvent *mev, anyOutput *o);

private:
	void (*ODexec)(int cmd, void *par, RECT *rec, anyOutput *o,
		void *data, int id);
};

class Listbox:public Dialog {
public:
	Listbox(tag_DlgObj *par, DlgInfo *des, RECT rec, char **list);
	~Listbox();
	bool Command(int cmd, void *tmpl, anyOutput *o);
	void DoPlot(anyOutput *o);
	bool Select(int x, int y, anyOutput *o);
	bool GetInt(int id, int *val);
	bool GetText(int id, char *txt, int size);
	void MBtrack(MouseEvent *mev, anyOutput *o);

private:
	ScrollBar *sb;
	anyOutput *bmp;
	char **strings;
	double spos;
	int ns, bmh, startY, cl;

	bool CreateBitMap(anyOutput *tmpl);
};

class Treeview:public Dialog {
public:
	Treeview(tag_DlgObj *par, DlgInfo *des, RECT rec, GraphObj *g);
	~Treeview();
	bool Command(int cmd, void *tmpl, anyOutput *o);
	void DoPlot(anyOutput *o);
	bool Select(int x, int y, anyOutput *o);
	bool GetInt(int id, int *val);
	void MBtrack(MouseEvent *mev, anyOutput *o);

private:
	ScrollBar *sb;
	anyOutput *bmp;
	double spos;
	int ns, bmh, bmw, startY, cl;
	GraphObj *go;
	ObjTree *ot;
};

class LinePat:public Dialog {
public:
	LinePat(tag_DlgObj *par, DlgInfo *desc, RECT rec, LineDEF *Line);
	void DoPlot(anyOutput *o);
	void MBtrack(MouseEvent *mev, anyOutput *o);

private:
	bool bDraw;
	DWORD *pPattern;
};

//prototypes TheDialog.cpp
bool UseRangeMark(DataObj *d, int type, char* =0L, char* =0L, char* =0L, char* =0L,
	char* =0L, char* =0L, char* =0L, char* =0L, char* =0L, char* =0L, char* =0L);
int com_StackDlg(int,DlgRoot*,AccRange**,int*,char***,int*,AccRange**,bool*,int*,int*,bool*);
DlgInfo *CompileDialog(char* tmpl, void **ptypes);

//prototypes ODbutton.cpp
void OD_DrawOrder(int cmd, void *par, RECT *rec, anyOutput *o, void *data, int id);
int ExecDrawOrderButt(GraphObj *parent, GraphObj *obj, int id);
void OD_PolygonStyleTempl(int cmd, void *par, RECT *rec, anyOutput *o, void *data, int id);
void OD_LineStyleTempl(int cmd, void *par, RECT *rec, anyOutput *o, void *data, int id);
void OD_BubbleTempl(int cmd, void *par, RECT *rec, anyOutput *o, void *data, int id);
void OD_ErrBarTempl(int cmd, void *par, RECT *rec, anyOutput *o, void *data, int id);
void OD_WhiskerTempl(int cmd, void *par, RECT *rec, anyOutput *o, void *data, int id);
void OD_PolarTempl(int cmd, void *par, RECT *rec, anyOutput *o, void *data, int id);
void OD_PieTempl(int cmd, void *par, RECT *rec, anyOutput *o, void *data, int id);
void OD_AxisDesc3D(int cmd, void *par, RECT *rec, anyOutput *o, void *data, int id);
void OD_BreakTempl(int cmd, void *par, RECT *rec, anyOutput *o, void *data, int id);
void OD_PlotTempl(int cmd, void *par, RECT *rec, anyOutput *o, void *data, int id);
void OD_AxisTempl(int cmd, void *par, RECT *rec, anyOutput *o, void *data, int id);
void OD_AxisTempl3D(int cmd, void *par, RECT *rec, anyOutput *o, void *data, int id);
void OD_NewAxisTempl(int cmd, void *par, RECT *rec, anyOutput *o, void *data, int id);
void OD_NewAxisTempl3D(int cmd, void *par, RECT *rec, anyOutput *o, void *data, int id);
