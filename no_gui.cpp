//no_gui.cpp, Copyright 2000-2007 R.Lackner
//
//    This file is part of RLPlot.
//
//    RLPlot is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    RLPlot is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with RLPlot; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// This modules contains code for builds with no graphical user interface.
// Builds using the GUI are compiled with use_gui.cpp instead.
//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
#include "rlplot.h"

extern char *name1, *name2;				//the filenames

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// STUBS: we do not need this objects or functions without GUI
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
dragHandle::dragHandle(GraphObj *par, int which):GraphObj(par, 0L)
{
}

dragHandle::~dragHandle()
{
}

void
dragHandle::DoPlot(anyOutput *o)
{
}

bool
dragHandle::Command(int cmd, void *tmpl, anyOutput *o)
{
	return false;
}

void *
dragHandle::ObjThere(int x, int y)
{
	return 0L;
}

void
dragHandle::Track(POINT *p, anyOutput *o)
{
}

dragRect::dragRect(GraphObj *par, int which):GraphObj(par, 0L)
{
}

dragRect::~dragRect()
{
}

void
dragRect::DoPlot(anyOutput *o)
{
}

void *
dragRect::ObjThere(int x, int y)
{
	return 0L;
}

bool
dragRect::Command(int cmd, void *tmpl, anyOutput *o)
{
	return false;
}

Drag3D::Drag3D(GraphObj *par):GraphObj(par, 0L)
{
}

Drag3D::~Drag3D()
{
}

void *
Drag3D::ObjThere(int x, int y)
{
	return 0L;
}

void
Drag3D::DoPlot(anyOutput *o)
{
}

FrmRect::FrmRect(GraphObj *par, fRECT *lim, fRECT *c, fRECT *chld):GraphObj(par, 0L)
{
	Id = Id;
}

FrmRect::~FrmRect()
{
}

double
FrmRect::GetSize(int select)
{
	return 0.0;
}

bool
FrmRect::SetSize(int select, double value)
{
	return false;
}

bool
FrmRect::SetColor(int select, DWORD col)
{
	return false;
}

void
FrmRect::DoMark(anyOutput *o, bool mark)
{
}

void
FrmRect::DoPlot(anyOutput *o)
{
}

bool
FrmRect::Command(int cmd, void *tmpl, anyOutput *o)
{
	return false;
}

void *
FrmRect::ObjThere(int x, int y)
{
	return 0L;
}

void
FrmRect::Track(POINT *p, anyOutput *o)
{
}

bool ShowLayers(GraphObj *root)
{
	return false;
}

bool GetBitmapRes(double *res, double *width, double *height, char *header)
{
	return false;
}

bool GetPaper(double *w, double *h)
{
	*w = *h = 1.0;
	return true;
}

bool Symbol::PropertyDlg()
{
	return false;
}

bool Bubble::PropertyDlg()
{
	return false;
}

bool Bar::PropertyDlg()
{
	return false;
}

bool DataLine::PropertyDlg()
{
	return false;
}

bool DataPolygon::PropertyDlg()
{
	return false;
}

bool RegLine::PropertyDlg()
{
	return false;
}

bool SDellipse::PropertyDlg()
{
	return false;
}

bool ErrorBar::PropertyDlg()
{
	return false;
}

bool Arrow::PropertyDlg()
{
	return false;
}

void *
Arrow::ObjThere(int x, int y)
{
	return 0L;
}

void
Arrow::Track(POINT *p, anyOutput *o)
{
}

bool Box::PropertyDlg()
{
	return false;
}

bool Whisker::PropertyDlg()
{
	return false;
}

bool DropLine::PropertyDlg()
{
	return false;
}

bool Sphere::PropertyDlg()
{
	return false;
}

bool Plane3D::PropertyDlg()
{
	return false;
}

bool Brick::PropertyDlg()
{
	return false;
}

bool DropLine3D::PropertyDlg()
{
	return false;
}

bool Arrow3D::PropertyDlg()
{
	return false;
}

bool Line3D::PropertyDlg()
{
	return false;
}

bool Label::PropertyDlg()
{
	return false;
}

void Label::ShowCursor(anyOutput *o)
{
}

bool Label::AddChar(int ci, anyOutput *o)
{
	return true;
}

void Label::CalcCursorPos(int x, int y, anyOutput *o)
{
}

bool TextFrame::PropertyDlg()
{
	return false;
}

void TextFrame::ShowCursor(anyOutput *o)
{
}

void TextFrame::CalcCursorPos(int x, int y, anyOutput *o)
{
}

bool segment::PropertyDlg()
{
	return false;
}

bool polyline::PropertyDlg()
{
	return false;
}

bool polygon::PropertyDlg()
{
	return false;
}

bool rectangle::PropertyDlg()
{
	return false;
}

bool PlotScatt::PropertyDlg()
{
	return false;
}

bool xyStat::PropertyDlg()
{
	return false;
}

bool Regression::PropertyDlg()
{
	return false;
}

bool FreqDist::PropertyDlg()
{
	return false;
}

bool BubblePlot::PropertyDlg()
{
	return false;
}

bool PolarPlot::PropertyDlg()
{
	return false;
}

bool PolarPlot::Config()
{
	return false;
}

bool BoxPlot::PropertyDlg()
{
	return false;
}

bool DensDisp::PropertyDlg()
{
	return false;
}

bool StackBar::PropertyDlg()
{
	return false;
}

bool Waterfall::PropertyDlg()
{
	return false;
}

bool MultiLines::PropertyDlg()
{
	return false;
}

bool PieChart::PropertyDlg()
{
	return false;
}

bool StarChart::PropertyDlg()
{
	return false;
}

bool Function::PropertyDlg()
{
	return false;
}

bool Grid3D::PropertyDlg()
{
	return false;
}

bool Grid3D::Configure()
{
	return false;
}

bool Scatt3D::PropertyDlg()
{
	return false;
}

bool FitFunc::PropertyDlg()
{
	return false;
}

bool NormQuant::PropertyDlg()
{
	return false;
}

bool ContourPlot::PropertyDlg()
{
	return false;
}

bool Plot3D::AddAxis()
{
	return false;
}

bool Plot3D::PropertyDlg()
{
	return false;
}

bool Plot3D::AddPlot(int)
{
	return false;
}

bool Chart25D::PropertyDlg()
{
	return false;
}

bool Ribbon25D::PropertyDlg()
{
	return false;
}

bool Func3D::PropertyDlg()
{
	return false;
}

bool FitFunc3D::PropertyDlg()
{
	return false;
}

bool BubblePlot3D::PropertyDlg()
{
	return false;
}

bool GridLine::PropertyDlg()
{
	return false;
}

bool Tick::PropertyDlg()
{
	return false;
}

void
Axis::DoMark(anyOutput *o, bool mark)
{
}

bool Axis::PropertyDlg()
{
	return false;
}

bool Graph::AddPlot(int family)
{
	return false;
}

bool Graph::PropertyDlg()
{
	return false;
}

bool Graph::Configure()
{
	return false;
}

bool Graph::AddAxis()
{
	return false;
}

bool Graph::ExecTool(MouseEvent *mev)
{
	return false;
}

bool Graph::MoveObj(int cmd, GraphObj *g)
{
	return false;
}

bool Graph::DoZoom(char *z)
{
	return false;
}

bool Page::Configure()
{
	return false;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// some more STUBS .....
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
char *SaveGraphAsName(char *oldname)
{
	return name2;
}

char *OpenGraphName(char *oldname)
{
	return 0L;
}

void HideTextCursor()
{
	return;
}

void HideTextCursorObj(anyOutput *out)
{
	return;
}

void ShowTextCursor(anyOutput *out, RECT *disp, DWORD color)
{
	return;
}

void InvalidateOutput(anyOutput *o)
{
	return;
}

void SuspendAnimation(anyOutput *o, bool bSusp)
{
	return;
}

anyOutput *NewDispClass(GraphObj *g)
{
	return 0L;
}

bool DelDispClass(anyOutput *w)
{
	return false;
}

anyOutput *NewBitmapClass(int w, int h, double hr, double vr)
{
	return 0L;
}

bool DelBitmapClass(anyOutput *w)
{
	return false;
}

