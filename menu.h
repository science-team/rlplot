//menu.h, (C) 2006-2008 R.Lackner
//
//    This file is part of RLPlot.
//
//    RLPlot is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    RLPlot is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with RLPlot; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// menu declarations
//

#define CM_OPEN        500
#define CM_SAVE        501
#define CM_EXIT        502
#define CM_NEWGRAPH    503
#define CM_NEWPAGE     504
#define CM_DELGRAPH    505
#define CM_ADDPLOT     506
#define CM_ABOUT       507
#define CM_ADDROWCOL   508
#define CM_COPYGRAPH   509
#define CM_REDRAW      510
#define CM_ZOOM25      511
#define CM_ZOOM50      512
#define CM_ZOOM100     513
#define CM_ZOOM200     514
#define CM_ZOOM400     515
#define CM_PRINT       516
#define CM_EXPORT      517
#define CM_DELOBJ      518
#define CM_DEFAULTS    519
#define CM_COPY        520
#define CM_PASTE       521
#define CM_UPDATE      522
#define CM_ADDAXIS     523
#define CM_UNDO        524
#define CM_ZOOMIN      525
#define CM_ZOOMOUT     526
#define CM_ZOOMFIT     527
#define CM_FILE1       528
#define CM_FILE2       529
#define CM_FILE3       530
#define CM_FILE4       531
#define CM_FILE5       532
#define CM_FILE6       533
#define CM_FILLRANGE   534
#define CM_CUT         535
#define CM_LEGEND      536
#define CM_LAYERS      537
#define CM_INSROW      538
#define CM_INSCOL      539
#define CM_DELROW      540
#define CM_DELCOL      541
#define CM_SAVEAS      542
#define CM_NEWINST     543

#define CM_T_STANDARD  550
#define CM_T_DRAW      551
#define CM_T_POLYLINE  552
#define CM_T_POLYGON   553
#define CM_T_RECTANGLE 554
#define CM_T_ROUNDREC  555
#define CM_T_ELLIPSE   556
#define CM_T_ARROW     557
#define CM_T_TEXT      558

#define CM_DELKEY      600
#define CM_LEFTARRKEY  601
#define CM_RIGHTARRKEY 602
#define CM_UPARRKEY    603
#define CM_DOWNARRKEY  604
#define CM_TAB         605
#define CM_SHTAB       606
#define CM_PGUP        607
#define CM_PGDOWN      608
#define CM_POS_FIRST   609
#define CM_POS_LAST    610
#define CM_SHLEFT      611
#define CM_SHRIGHT     612
#define CM_SHUP        613
#define CM_SHDOWN      614
#define CM_SHPGUP      615
#define CM_SHPGDOWN    616

#define CM_SMPLSTAT    650
#define CM_REPCMEANS   651
#define CM_REPANOV     652
#define CM_REPTWANOV   653
#define CM_REPFRIEDM   654
#define CM_REPTWANR    655
#define CM_REPKRUSKAL  656
#define CM_REPREGR     657
#define CM_ROBUSTLINE  658
#define CM_CORRELM     659
#define CM_CORRELT     660
#define CM_REPTWOWAY   661
#define CM_REPBDANOV   662
