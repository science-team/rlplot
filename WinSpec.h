//WinSpec.h, Copyright (c) 2000-2008 R.Lackner
//
//    This file is part of RLPlot.
//
//    RLPlot is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    RLPlot is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with RLPlot; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
class BitMapWin:public anyOutput{
public:
	HBITMAP scr;
	GraphObj *go;
	HDC memDC;
	HPEN hPen, oldPen;
	HBRUSH hBrush, oldBrush;
	HFONT hFont;
	HatchOut *hgo;
	RECT tr_rec;			//rectangle for transparency
	anyOutput *tr_out;		//transparency source class

	BitMapWin(GraphObj *g, HWND hw);
	BitMapWin(int w, int h, double hr, double vr);
	BitMapWin(GraphObj *g);
	~BitMapWin();
	bool SetLine(LineDEF *lDef);
	bool SetFill(FillDEF *fill);
	bool SetTextSpec(TextDEF *set);
	virtual bool Erase(DWORD Color);
	virtual bool StartPage() {return true;};
	bool CopyBitmap(int x, int y, anyOutput* src, int sx, int sy,
		int sw, int sh, bool invert);
	bool oGetTextExtent(char *text, int cb, int *width, int *height);
	bool oGetTextExtentW(w_char *text, int cb, int *width, int *height);
	bool oGetPix(int x, int y, DWORD *col);
	bool oDrawIcon(int type, int x, int y);
	bool oCircle(int x1, int y1, int x2, int y2, char* nam = 0L);
	bool oPolyline(POINT * pts, int cp, char *nam = 0L);
	bool oRectangle(int x1, int y1, int x2, int y2, char* nam = 0L);
	bool oSolidLine(POINT *p);
	bool oTextOut(int x, int y, char *txt, int cb);
	bool oTextOutW(int x, int y, w_char *txt, int cb);
	bool oPolygon(POINT *pts, int cp, char *nam = 0L);
	void DoTransparency(DWORD color);
};

class OutputWin:public BitMapWin{
public:
	HWND hWnd;
	HDC hdc;

	OutputWin(GraphObj *g, HWND hw);
	~OutputWin();
	bool ActualSize(RECT *rc);
	void Focus(){if(hWnd) SetFocus(hWnd);};
	void Caption(char *txt, bool bModified);
	void MouseCursor(int cid, bool force);
	bool SetScroll(bool isVert, int iMin, int iMax, int iPSize, int iPos);
	bool Erase(DWORD Color);
	bool StartPage();
	bool EndPage();
	bool UpdateRect(RECT *rc, bool invert);
	bool UpdateRect(HDC hdc, RECT rc);
	void ShowBitmap(int x, int y, anyOutput* src);
	void ShowLine(POINT * pts, int cp, DWORD color);
	void ShowEllipse(POINT p1, POINT p2, DWORD color); 
	bool SetMenu(int type);
	void CheckMenu(int mid, bool check);
	void FileHistory();

private:
	void CreateNewWindow(void *g);
};

class WinCopyWMF:public anyOutput {
public:
	WinCopyWMF(GraphObj *g, char* file_wmf, char *file_emf);
	~WinCopyWMF();
	bool SetLine(LineDEF *lDef);
	bool SetFill(FillDEF *fill);
	bool SetTextSpec(TextDEF *set);
	bool oGetTextExtent(char *text, int cb, int *width, int *height);
	bool oGetTextExtentW(w_char *text, int cb, int *width, int *height);
	bool StartPage();
	bool EndPage();
	bool oCircle(int x1, int y1, int x2, int y2, char* nam = 0L);
	bool oPolyline(POINT * pts, int cp, char *nam = 0L);
	bool oRectangle(int x1, int y1, int x2, int y2, char *nam = 0L);
	bool oSolidLine(POINT *p);
	bool oTextOut(int x, int y, char *txt, int cb);
	bool oTextOutW(int x, int y, w_char *txt, int cb);
	bool oPolygon(POINT *pts, int cp, char *nam = 0L);

private:
	int bott_y;
	GraphObj *go;
	HDC hdc;
	HPEN hPen;
	HBRUSH hBrush;
	HFONT hFont;
	HatchOut *hgo;
	char *wmf_file, *emf_file;
};

class PrintWin:public anyOutput{
public:
	PrintWin();
	~PrintWin();
	bool SetLine(LineDEF *lDef);
	bool SetFill(FillDEF *fill);
	bool SetTextSpec(TextDEF *set);
	bool oGetTextExtent(char *text, int cb, int *width, int *height);
	bool oGetTextExtentW(w_char *text, int cb, int *width, int *height);
	bool StartPage();
	bool EndPage();
	bool CopyBitmap(int x, int y, anyOutput* src, int sx, int sy,
		int sw, int sh, bool invert);
	bool Eject();
	bool oCircle(int x1, int y1, int x2, int y2, char* nam = 0L);
	bool oPolyline(POINT * pts, int cp, char *nam = 0L);
	bool oRectangle(int x1, int y1, int x2, int y2, char *nam = 0L);
	bool oSolidLine(POINT *p);
	bool oTextOut(int x, int y, char *txt, int cb);
	bool oTextOutW(int x, int y, w_char *txt, int cb);
	bool oPolygon(POINT *pts, int cp, char *nam = 0L);

private:
	HPEN hPen;
	HBRUSH hBrush;
	HFONT hFont;
	HatchOut *hgo;
	char *PrintDevice, *PrintDriver, *PrintPort;
	HDC hDC;
};
