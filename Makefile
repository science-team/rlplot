# Makefile, Copyright 2002-2008 R.Lackner
# 
#
#    This file is part of RLPlot.
#
#    RLPlot is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    RLPlot is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RLPlot; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
CC = g++
X11LIBS = -lX11 -lm
SRCDIR = ./

#######
#######   Declarations for Qt3
QT3DIR = /usr/lib/qt-3.3
QT3MOC = /usr/lib/qt-3.3/bin/moc
MOC3FLAGS =
QT3CFLAGS = "-I$(QT3DIR)/include -pipe -O2"
QT3H = QT3_Spec.h
QT3LIBS = "-L$(QT3DIR)/lib -L/usr/X11R6/lib -lqt-mt"
#
#   If above declarations don't work for Qt3 try the following ...
#QT3DIR = /usr/share/qt-3
#QT3DIR = /usr/share/qt3
#QT3MOC = /usr/share/qt-3/bin/moc
#QT3MOC = /usr/share/qt3/bin/moc

#######
#######   Declarations for Qt4
QT4DIR = /usr/lib/qt4
QT4MOC = /usr/lib/qt4/bin/moc-qt4
MOC4FLAGS = "MOCFLAGS=-DQT_VERSION=0x040000"
QT4CFLAGS = "-I/usr/include/Qt -pipe -O2"
QT4H = QT_Spec.h
QT4LIBS = "-L$(QT4DIR)/lib -L/usr/X11R6/lib -lQtCore -lQtGui"
#
#   If above declarations don't work for Qt4 try the following ...
#QT4DIR = /usr/share/qt4
#QT4MOC = /usr/share/qt4/bin/moc
#QT4MOC = /usr/lib/qt4/bin/moc
#QT4CFLAGS = "-I/usr/include/qt4/Qt -I/usr/include/qt4 -pipe -O2"
#QT4CFLAGS = "-I/usr/lib/qt4/include -I/usr/lib/qt4/include/Qt -pipe -O2"

#######
GENOBJ = exprlp.o rlplot.o Output.o Utils.o UtilObj.o\
 Fileio.o Export.o PlotObs.o Axes.o mfcalc.o rlp_math.o no_gui.o

OBJECTS = moc_QT_Spec.o QT_Spec.o Output.o Utils.o UtilObj.o\
 TheDialog.o rlplot.o Fileio.o PropertyDlg.o spreadwi.o\
 Export.o PlotObs.o Axes.o ODbuttons.o mfcalc.o rlp_math.o use_gui.o\
 reports.o

#######
all:
	make Qt4
	make exprlp

Qt4:
	make rlplot QTDIR=$(QT4DIR) MOC=$(QT4MOC) CFLAGS=$(QT4CFLAGS) QTH=$(QT4H) $(MOC4FLAGS) LIBS=$(QT4LIBS)

Qt3:
	make rlplot QTDIR=$(QT3DIR) MOC=$(QT3MOC) CFLAGS=$(QT3CFLAGS) QTH=$(QT3H) $(MOC3FLAGS) LIBS=$(QT3LIBS)

rlplot: $(OBJECTS)
	$(CC) $(LIBS) -o rlplot $(OBJECTS) $(QTLIBS) $(X11LIBS)

exprlp: $(GENOBJ)
	$(CC) -o exprlp $(GENOBJ)

clean:
	rm -f *.o *~
	rm -f moc_QT_Spec.cpp
	rm -f rlplot
	rm -f exprlp

####### Compile

moc_QT_Spec.o: moc_QT_Spec.cpp $(SRCDIR)QT_Spec.h
	$(CC) $(CFLAGS) -o $@ -c $<

moc_QT_Spec.cpp: $(SRCDIR)QT_Spec.h
	$(MOC) $(SRCDIR)$(QTH) -o moc_QT_Spec.cpp $(MOCFLAGS)

mfcalc.o: $(SRCDIR)mfcalc.cpp
	$(CC) $(CFLAGS) -o $@ -c $<

rlp_math.o: $(SRCDIR)rlp_math.cpp
	$(CC) $(CFLAGS) -o $@ -c $<

#$(SRCDIR)mfcalc.cpp: $(SRCDIR)mfcalc.y
#	bison -l -o $@ $<

exprlp.o: $(SRCDIR)exprlp.cpp
	$(CC) $(CFLAGS) -o $@ -c $<

QT_Spec.o: $(SRCDIR)QT_Spec.cpp $(SRCDIR)QT_Spec.h $(SRCDIR)rlplot.h $(SRCDIR)menu.h
	$(CC) $(CFLAGS) -o $@ -c $<

Output.o: $(SRCDIR)Output.cpp $(SRCDIR)rlplot.h
	$(CC) $(CFLAGS) -o $@ -c $<

Utils.o: $(SRCDIR)Utils.cpp $(SRCDIR)rlplot.h
	$(CC) $(CFLAGS) -o $@ -c $<

UtilObj.o: $(SRCDIR)UtilObj.cpp $(SRCDIR)rlplot.h
	$(CC) $(CFLAGS) -o $@ -c $<

TheDialog.o: $(SRCDIR)TheDialog.cpp $(SRCDIR)TheDialog.h $(SRCDIR)rlplot.h
	$(CC) $(CFLAGS) -o $@ -c $<

rlplot.o: $(SRCDIR)rlplot.cpp $(SRCDIR)rlplot.h
	$(CC) $(CFLAGS) -o $@ -c $<

Fileio.o: $(SRCDIR)Fileio.cpp $(SRCDIR)rlplot.h
	$(CC) $(CFLAGS) -o $@ -c $<

PropertyDlg.o: $(SRCDIR)PropertyDlg.cpp $(SRCDIR)rlplot.h $(SRCDIR)TheDialog.h
	$(CC) $(CFLAGS) -o $@ -c $<

spreadwi.o: $(SRCDIR)spreadwi.cpp $(SRCDIR)rlplot.h
	$(CC) $(CFLAGS) -o $@ -c $<

Export.o: $(SRCDIR)Export.cpp $(SRCDIR)rlplot.h
	$(CC) $(CFLAGS) -o $@ -c $<

PlotObs.o: $(SRCDIR)PlotObs.cpp $(SRCDIR)rlplot.h
	$(CC) $(CFLAGS) -o $@ -c $<

ODbuttons.o: $(SRCDIR)ODbuttons.cpp $(SRCDIR)rlplot.h
	$(CC) $(CFLAGS) -o $@ -c $<

Axes.o: $(SRCDIR)Axes.cpp $(SRCDIR)rlplot.h
	$(CC) $(CFLAGS) -o $@ -c $<

no_gui.o: $(SRCDIR)no_gui.cpp $(SRCDIR)rlplot.h
	$(CC) $(CFLAGS) -o $@ -c $<

use_gui.o: $(SRCDIR)use_gui.cpp $(SRCDIR)rlplot.h
	$(CC) $(CFLAGS) -o $@ -c $<

reports.o: $(SRCDIR)reports.cpp  $(SRCDIR)TheDialog.h $(SRCDIR)rlplot.h
	$(CC) $(CFLAGS) -o $@ -c $<

